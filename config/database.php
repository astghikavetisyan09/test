<?php

return [

   'default' => 'mysql',
   'migrations' => 'migrations',
   'connections' => [
        'mysql' => [
            'driver'    => 'mysql',
            'host'      =>  env('DB_HOST', 'localhost'),
            'port'      => env('DB_PORT', '3306'),
            'database'  => env('DB_DATABASE', 'task'),
            'username'  => env('DB_USERNAME','root'),
            'password'  => env(''),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
         ],

        'mysql2' => [
            'driver'    => 'mysql',
            'host'      =>  env('DB_HOST', 'localhost'),
            'port'      => env('DB_PORT', '3306'),
            'database'  => env('DB_DATABASE', 'testing_database'),
            'username'  => env('DB_USERNAME','root'),
            'password'  => env(''),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],
    ],
];