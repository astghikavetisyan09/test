<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


$router->get('/', ['uses' => 'Controller@index']);
$router->post('/addUser', ['uses' => 'Controller@addUser']);
$router->post('/deleteUser', ['uses' => 'Controller@deleteUser']);
$router->post('/editUser', ['uses' => 'Controller@editUser']);
$router->post('/addTeam', ['uses' => 'Controller@addTeam']);
$router->post('/deleteTeam', ['uses' => 'Controller@deleteTeam']);
$router->post('/editTeam', ['uses' => 'Controller@editTeam']);
$router->post('/assignUser', ['uses' => 'Controller@assignUser']);