<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;


class UserAssignTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A basic test example.
     *
     * @return void
     */

    public function testUserAssign()
    {
          $this->post('/assignUser', ['team_id' => 1, 'user' => '1', 'role' => '1']);
          $this->seestatusCode(200);
    }

}