<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class TeamDeleteTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testTeamDelete()
    {   

        $id = DB::table('teams')->insertGetId(
            ['title' => 'titlee']
        );
          $this->post('/deleteTeam', ['id' => $id]);
          $this->seestatusCode(200);
    }
}
