<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;


class UserDeleteTest extends TestCase
{
  use DatabaseTransactions;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUserDelete()
    {

        $id = DB::table('users')->insertGetId(
            ['name' => 'name', 'email' => 'john@example.com']
        );
          $this->post('/deleteUser', ['email' => 'john@example.com']);
          $this->seestatusCode(200);
    }
}