<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class UserAddTest extends TestCase
{
     use DatabaseTransactions;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUserAdd()
    {
          $this->post('/addUser', ['name' => 'sally', 'email' => 'testemail@gmail.com'])->assertEquals('', $this->response->getContent());
          // DB::table('users')->where(['name' => 'sally', 'email' => 'testemail@gmail.com'])->delete();
    }
}