<html>
<head>
	<title></title>
	<meta name="csrf-token" content="<?php echo app('session')->token() ?>" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>
	<div id="container">
		<div id="users">
			<h1>Add User</h1>
			<p id="UserError"> </p>
			<input type="text" name="" id="name" placeholder="name">
			<input type="email" name="" id="email" placeholder="email">
			<button id="addUser">
				Add User 
			</button>
			<h1>All Users</h1>
			<?php foreach ($data as $user) { ?>
			<div>
				<input type="hidden" value="<?= $user->id ?>"/>
				<h3><?=$user->name?> </h3>
				<p><?=$user->email?> </p>
				<button id="<?= $user->id?>" class="userDelete">Delete user</button>
				<button id="<?= $user->id?>" class="editUser">Edit user</button>
			</div>
			<?php }  ?>
		</div>
		<div id="teams">
			<h1>Add Teams</h1>
			<p id="TeamError"></p>
			<input type="text" name="" id="title" placeholder="title">
			<button id="addTeam">
				Add Team 
			</button>
			<h1>All Teams</h1>
			<?php foreach ($teams as $team) { ?>
				<div>
					<h3><?=$team->title?></h3>
					<button id="team<?= $team->id?>" class="teamDelete">Delete team</button>
					<button id="team<?= $team->id?>" class="editTeam">Edit team</button>
					<button id="team<?= $team->id?>" class="assignUser">Assign User</button>
					<p id="assignError"></p>
					<select style="display: none" id="selectOwner">
						<option value="0">Select owner</option>
						<?php foreach ($data as $user) { ?>
							<option value="<?=$user->id ?>"><?= $user->name ?></option>
						<?php } ?>
					</select>
					<select style="display: none" id="userRole">
						<option value="0">Select user role</option>
							<option value="1">User</option>
							<option value="2">Owner</option>
					</select>
				</div>
			<?php }  ?>
		</div>
	</body>
	</html>



	<script type="text/javascript">
		$("#addUser").click(function(){
			let name = $("#name").val()
			let email = $("#email").val()
			$.ajax({
				type:"POST",
				url:"/addUser",
				data:{'_token':$('meta[name="csrf-token"]').attr('content'), name:name, email:email},
				success:function(r){
					if(r.name){
						document.getElementById("UserError").innerHTML = r.name;
					}else if(r.email){
						document.getElementById("UserError").innerHTML = r.email;
					}
					else{
						let a = (`
							<div>
							<input type="hidden" value="${r}"/>
						<h3>${name}</h3>
						<p>${email}</p>
						<button class="userDelete" id=${r}>Delete user</button>
						<button class="editUser" id=${r}>Edit user</button>
					</div>`)
						$('#users').append(a)
						$("#name").val('')
						$("#email").val('')
					}
				}
			})
		})
		$(document).on('click','.userDelete',function(){
			let id = $(this).attr('id');
			let email = $(this).parent().find('p').html()
			$.ajax({
				type: "POST",
				url: '/deleteUser',
				data: {'_token':$('meta[name="csrf-token"]').attr('content'), email:email},
				success: function(r){
					$('#'+id).parent().css('display','none');	
				}
			})
		})
		$(document).on('click','.editUser',function(){
			let id = $(this).attr('id');
			let a=(`<p id="EditError"></p><input type="text" name="" id="Ename" placeholder="name"><input type="email" name="" id="Eemail" placeholder="email"><button class="EditUser" id="${id}" >Save</button> `);
			$(this).after(a)
			$(this).css('display','none');
		})
		$(document).on('click','.EditUser',function(){
			let id = $(this).attr('id');
			let name= $("#Ename").val();
			let email = $("#Eemail").val();
			$.ajax({
				type:"POST",
				url:"/editUser",
				data: {'_token':$('meta[name="csrf-token"]').attr('content'), id:id, name:name, email:email},
				success: function(r){
					if(r.email){
						document.getElementById("EditError").innerHTML = r.email;
					}
				}
			})
		})
		$("#addTeam").click(function(){
			let title = $("#title").val()
				$.ajax({
					type:"POST",
					url:"/addTeam",
					data:{'_token':$('meta[name="csrf-token"]').attr('content'), title: title},
					success:function(r){
						if(r.title){
							document.getElementById("TeamError").innerHTML = r.title;
						}
						else{
							r=JSON.parse(r)
							let a=(`
									<div>
						<h3>${title}</h3>
						<button id="team${r['id']}" class="teamDelete">Delete team</button>
						<button id="team${r['id']}" class="editTeam">Edit team</button>
						<button id="team${r['id']}" class="assignUser">Assign User</button>
						<p id="assignError"></p>
						<select style="display: none" id="selectOwner">
							<option value="0">Select owner</option>`)
							for(let i=0;i<r['users'].length;i++){
							a+=(`<option value="${r['users'][i]['id']}">${r['users'][i]['name']}</option>`)
						}
						a+=(`</select>
							<select style="display: none" id="userRole">
						<option value="0">Select user role</option>
							<option value="1">User</option>
							<option value="2">Owner</option>
					</select>
					</div>`)
							$('#teams').append(a)
							$("#title").val('')
						}
					}
				})
		})
		$(document).on('click','.teamDelete',function(){
			let id = $(this).attr('id');
			id = id.slice(4, id.length)
			$.ajax({
				type: "POST",
				url: '/deleteTeam',
				data: {'_token':$('meta[name="csrf-token"]').attr('content'), id:id},
				success: function(r){
					$('#team'+id).parent().css('display', 'none');
				}
			})
		})
		$(document).on('click','.editTeam',function(){
			let id = $(this).attr('id');
			id = id.slice(4, id.length)
			$(this).parent().find('#selectOwner').css('display','block');
			let a=(`<p id="TitleError"></p><input type="text" name="" id="Etitle" placeholder="title"><button class="EditTeam" id="${id}" >Save</button> `);
			$(this).after(a)
			$(this).css('display','none')
		})
		$(document).on('click','.EditTeam',function(){
			let id = $(this).attr('id');
			let Etitle= $("#Etitle").val();
			let owner = $(this).parent().find('#selectOwner').val();
			$.ajax({
				type:"POST",
				url:"/editTeam",
				data: {'_token':$('meta[name="csrf-token"]').attr('content'), id:id, title:Etitle, owner:owner},
				success: function(r){
					if(r.title){
							document.getElementById("TitleError").innerHTML = r.title;
						}
				}
			})
		})
		$(document).on('click', '.assignUser', function(){
			let id = $(this).attr('id');
			id = id.slice(4, id.length);
			$(this).parent().find('#selectOwner').css('display','block');
			$(this).parent().find('#userRole').css('display','block');
			let a=(`
				<button class="assign" id="${id}" >Save</button>
				`)
			$(this).after(a)
			$(this).css('display','none');
		})
		$(document).on('click', '.assign', function(){
			let id = $(this).attr('id');
			let user = $(this).parent().find('#selectOwner').val()
			let role = $(this).parent().find('#userRole').val()
			$.ajax({
				type:"POST",
				url:"/assignUser",
				data: {'_token':$('meta[name="csrf-token"]').attr('content'), team_id:id, user:user, role:role},
				success: function(r){
					if(r.role){
						let text = "Please select user's role";
							document.getElementById("assignError").innerHTML = text;
						}
					if(r.user){
						let text = "Please select user";
						document.getElementById("assignError").innerHTML = text;
					}
				}
			})
		})

	</script>

	<style type="text/css">
		#UserError{
			color: red;
		}
		#TeamError{
			color:red;
		}
		#EditError{
			color: red;
		}
		#TitleError{
			color: red;
		}
		#assignError{
			color: red;
		}
		#container{
			display: flex;
		}
		#container div{
			width: 50%;
		}
	</style>