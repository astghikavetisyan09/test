<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use DB;

class List extends Model  
{
    use Authenticatable, Authorizable, HasFactory;

    public function user(){
        return $this->morphToMany('App\Models\User');
    }
    public function teams(){
            return $this->morphToMany('App\Models\Team');
    }
    public function getAll(){
        $query = DB::connection('mysql')->select("select * from list");
        return $query;
    }

}