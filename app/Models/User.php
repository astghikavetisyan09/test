<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
Use DB;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function teams()
        {
            return $this->morphToMany('App\Models\Team');
        }
    protected $fillable = [
        'name', 'email',
    ];

   
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    public function read(){
        $query = DB::connection('mysql')->select("Select * from users");
        return $query;
    }

}
 