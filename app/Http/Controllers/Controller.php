<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use App\Models\User;
use App\Models\Team;
use Illuminate\Http\Request;
// use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Validator;
use DB;
class Controller extends BaseController
{
    //
    public function index(){
    	$user = new User();
    	$team = new Team();
    	$data = $user->read();
    	$teams = $team->read();
    	return view('index', compact('data', 'teams'));
    }
    public function addUser(Request $request){
    	$validator = Validator::make($request->all(), [
    		'name' => 'required',
    		'email' =>'required|email|unique:users',
    	]);
    	if ($validator->fails()) return $validator->errors();
    
    	$user = new User();
    	$user->name = $request['name'];
	    $user->email = $request['email'];
	    $user->save();
	    echo json_encode($user->id);
    }
    public function deleteUser(Request $request){
    	$user = new User();
    	$user->where('email', $request['email'])->delete();
    }
    public function editUser(Request $request){
    	$validator = Validator::make($request->all(), [
    		'email' =>'required|email|unique:users',
    		'name' => 'required'
    	]);
    	$errors = $validator->messages()->getMessages();
    	
    	$user = new User();
    	if ($validator->fails()) {
    		if (isset($errors['email']) && !isset($errors['name'])) {
    			$user->where('id', $request['id'])->update(['name' => $request['name']]);
    		}
    		else if (!isset($errors['email']) && isset($errors['name'])) {
    			$user->where('id', $request['id'])->update(['email' => $request['email']]);
    		}
    	}
    	else{
    		$user->where('id', $request['id'])->update(['name'=>$request['name'], 'email' => $request['email']]);
    	}
    }

    public function addTeam(Request $request){
    	$validator = Validator::make($request->all(),[
    		'title' => 'required',
    	]);
    	if ($validator->fails()) return $validator->errors();

    	$team = new Team();
    	$team->title = $request['title'];
    	$team->save();
    	$data['id']=$team->id;
    	$user = New User();
    	$data['users'] = $user->read();
    	echo json_encode($data);
    }

    public function deleteTeam(Request $request){
    	$team = new Team();
    	$team->find($request['id'])->delete();
    }

    public function editTeam(Request $request){
    	$validator = Validator::make($request->all(),[
    		'title' => 'required',
    	]);
    	if ($validator->fails()) return $validator->errors();

    	$team = new Team();
    	$team->where('id', $request['id'])->update(['title' => $request['title']]);
    }

    public function assignUser(Request $request){
    	$validator = Validator::make($request->all(),[
    		'user' => 'numeric|min:1',
    		'role' => 'numeric|min:1',
    	]);
    	if($validator->fails()) return $validator->errors();
    	
    	DB::insert('insert into list (user_id, team_id, role_id) values ( '.$request['user'].','.$request['team_id'].','.$request['role'].' )');
    }
}
